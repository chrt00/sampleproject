﻿/** nedbThunk.js **/
'use strict';

/**
 * @database nedb database
 */
module.exports = NedbThunk;
function NedbThunk(database){
    this._db = database;
}

NedbThunk.prototype.find = function (query) {
    var db = this._db;
    return function (done) {
        db.find(query, function (err, result) {
            done(err, result);
        });
    }
};
NedbThunk.prototype.findOne = function (query) {
    var db = this._db;
    return function (done) {
        db.findOne(query, function (err, result) {
            done(err, result);
        });
    }
};
NedbThunk.prototype.create = function (doc) {
    var db = this._db;
    return function (done) {
        db.insert(doc, function (err, newDoc) {
            done(err, newDoc);
        });
    }
}
NedbThunk.prototype.update = function (query, update, options) {
    var db = this._db;
    return function (done) {
        db.update(query, update, options, function (err, numUpdated) {
            done(err, numUpdated);
        });
    }
};
NedbThunk.prototype.remove = function (query, options) {
    var db = this._db;
    return function (done) {
        db.remove(query, options, function (err, numRemoved) {
            done(err, numRemoved);
        });
    }
};