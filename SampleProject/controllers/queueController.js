﻿/** queueController.js **/
'use strict';
var request = require('request');

var Error = require('../error.js');

var queueDbThunks;      // static variable
var consumerDbThunks;   // static variable
var backgroundWorker;   // static variable

module.exports = QueueController;
function QueueController(queueDb, consumerDb, worker) {
    consumerDbThunks = consumerDb;
    queueDbThunks = queueDb;
    backgroundWorker = worker;
}

QueueController.prototype.list = function*() {
    this.body = yield queueDbThunks.find({});
};
QueueController.prototype.create = function*() {
    var requestBody = this.request.body;
    if (requestBody.constructor === Array) {
        this.throw(new Error('Cannot accept JSON array, pass object instead').toString(), 400);
    } else {
        // TODO validate name exists
        var queue = { name: requestBody.name }
        this.body = yield queueDbThunks.create(queue);
    }
};
QueueController.prototype.edit = function*(id) {
    var requestBody = this.request.body;
    // Validations
    // check it is an object
    if (requestBody.constructor === Array) {
        this.throw(new Error('Cannot accept JSON array, pass object instead').toString(), 400);
    } else {
        // TODO check for blank name?
        var name = requestBody.name;
        var updated = yield queueDbThunks.update({ _id: id }, { $set: { name: name } }, {});
        if (updated == 1) {
            this.body = yield queueDbThunks.findOne({ _id: id });
        } else if (updated == 0) {
            this.throw(new Error('Not found').toString(), 404);
        } else {
            // updated multiple 'unique' records, report/log this error
            this.throw(new Error('Server Error').toString(), 500);
        }
    }
};
QueueController.prototype.remove = function*(id) {
    var records = yield queueDbThunks.find({ _id: id });
    if (records.length == 0) {
        this.throw(new Error('Not found').toString(), 404);
    } else if (records.length > 1) {
        // duplicate records log this error
        this.throw(new Error('Server Error').toString(), 500);
    } else {
        var removed = yield queueDbThunks.remove({ _id: id }, {});
        if (removed > 1) {
                // report duplicate records, we updated multiple records
        }
        var removedConsumers = yield consumerDbThunks.remove({ queue_id: id }, { multi: true });
        var consumers = yield consumerDbThunks.find({});
        this.body = records[0];
    }
};
QueueController.prototype.publish = function*(id) {
    var requestBody = this.request.body;
    if (requestBody.constructor === Array) {
        this.throw(new Error('Cannot accept JSON array, pass object instead').toString(), 400);
    } else {
        var queue = yield queueDbThunks.find({ _id: id });
        var consumers = yield consumerDbThunks.find({ queue_id: id });
        var message = requestBody.message;
        /* 
         * Ideally this HTTP POST operation would be posted to a background service
         * to send the request so that our REST service has less load
         */
        consumers.map(function (item) {
            var url = item.callback_url;
            var body = { message: message };
            if (backgroundWorker) {
                backgroundWorker.send({ url: url, message: JSON.stringify(body) });
            } else {
                console.log('ERROR: background worker unavailable');
            }
        });
    }
    this.body = JSON.stringify({ status: 'ok' });
};