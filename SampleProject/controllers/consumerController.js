﻿/** consumerController.js **/
'use strict';

var validUrl = require('valid-url');

var Error = require('../error.js');

var consumerDbThunks;   // static variable
var queueDbThunks;      // static variable

module.exports = ConsumerController;
function ConsumerController(queueDb, consumerDb) {
    consumerDbThunks = consumerDb;
    queueDbThunks = queueDb;
}

ConsumerController.prototype.list = function*(id) {
    var queue = yield queueDbThunks.findOne({ _id: id });
    if (queue) {
        this.body = yield consumerDbThunks.find({ queue_id: id });
    } else {
        this.throw(new Error('Queue not found').toString(), 404);
    }
};

ConsumerController.prototype.listAll = function*(id) {
    this.body = yield consumerDbThunks.find({});
};

ConsumerController.prototype.create = function*(id) {
    var requestBody = this.request.body;
    var queue = yield queueDbThunks.findOne({ _id: id });
    var existingUrl = yield consumerDbThunks.find({ callback_url: requestBody.callback_url })
    if (existingUrl.length != 0) {
        this.throw(new Error('Duplicate callback url').toString(), 400);
    } else if (!validUrl.isHttpUri(requestBody.callback_url)) {
        this.throw(new Error('Invalid callback url').toString(), 400);
    } else if (queue) {
        var consumer = {
            queue_id: id,
            callback_url: requestBody.callback_url
        };
        this.body = yield consumerDbThunks.create(consumer);
    } else {
        this.throw(new Error('Queue not found').toString(), 404);
    }
};

ConsumerController.prototype.remove = function*(id, consumer_id) {
    // var queue = yield queueDbThunks.findOne({ _id: id });
    var records = yield consumerDbThunks.find({ _id: consumer_id, queue_id: id });
    if (records.length == 0) {
        this.throw(new Error('Not found').toString(), 404);
    } else if (records.length > 1) {
        // duplicate records log this error
        this.throw(new Error('Server Error').toString(), 500);
    } else {
        var removed = yield consumerDbThunks.remove({ _id: consumer_id, queue_id: id }, {});
        if (removed > 1) {
                // report duplicate records, we updated multiple records
        }
        // this.body = records[0];
        this.body = "{ status: 'ok'}";
    }
};