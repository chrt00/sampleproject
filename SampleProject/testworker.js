﻿'use strict'
/** 
 * testworker.js
 * Simple test fixture for background queue
 */
const fork = require('child-process-debug').fork;
var worker = fork('worker.js');
setTimeout(function (){
    worker.send({ url: 'http://google.com', message: 'Sorry google' });
}, 0)
setTimeout(function () {
    worker.send({ url: 'http://requestb.in/1hdqngh1', message: 'test message' });
}, 0)