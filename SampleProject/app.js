﻿'use strict';

/** Child process includes **/

const fork = require('child-process-debug').fork;
var worker = fork('worker.js');

/** Koa includes **/

var koa = require('koa');
var router = require('koa-route');
var bodyParser = require('koa-bodyparser');

var app = koa();
app.use(bodyParser({
    strict: true,
    onerror: function (err, ctx) {
        ctx.throw(new Error('body parse error').toString(), 422);
    }
}));

/** Database + db utils initialization **/

var Datastore = require('nedb');

var queueDb = new Datastore();
var consumerDb = new Datastore();

var NedbThunk = require('./nedbThunk.js');
var queueDbThunks = new NedbThunk(queueDb);
var consumerDbThunks = new NedbThunk(consumerDb);

/** Misc. utilities **/
var Error = require('./error.js');

// TODO put headers/logging into initializers/config file
// logger + x-response-time

app.use(function*(next) {
    var start = new Date;
    yield next;
    var ms = new Date- start;
    console.log('%s %s - %s', this.method, this.url, ms);
    this.set('X-Response-Time', ms + 'ms');
});

/** Seed database on first run **/

var queueList = [{ name: 'queue01' }, { name: 'queue02' }];
queueList.map(function (item) {
    queueDb.insert(item);
});
queueDb.find({}, function (err, queueListWithIds) {
    for (var i = 0; i < queueListWithIds.length; i++) {      
        for ( var j = 0; j < 1; j++) {
            var consumer = {
                // name: 'consumer' + i + j,
                queue_id: queueListWithIds[i]._id,
                callback_url: 'http://requestb.in/1bx1plz1'
            }
            consumerDb.insert(consumer, function (err, result) {
                var e = err;
            });
        }
        consumerDb.insert({
            queue_id: queueListWithIds[i]._id,
            callback_url: 'http://requestb.in/1hdqngh1'
        });
        consumerDb.insert({
            queue_id: queueListWithIds[i]._id,
            callback_url: 'http://google.com'
        });
    }
});

var hello = function*() {
    this.body = 'Hello World';
};

/** Controllers **/

var QueueController = require('./controllers/queueController.js');
var queueController = new QueueController(queueDbThunks, consumerDbThunks, worker);

var ConsumerController = require('./controllers/consumerController.js');
var consumerController = new ConsumerController(queueDbThunks, consumerDbThunks);

// Routing

app.use(router.get('/', hello));
app.use(router.post('/', hello));
app.use(router.get('/queues', queueController.list));
app.use(router.post('/queues', queueController.create));
app.use(router.put('/queues/:id', queueController.edit));
app.use(router.delete('/queues/:id', queueController.remove));

app.use(router.post('/queues/:id/messages', queueController.publish));

app.use(router.post('/queues/:id/consumers', consumerController.create));
app.use(router.get('/queues/:id/consumers', consumerController.list));
app.use(router.get('/consumers', consumerController.listAll));
app.use(router.delete('/queues/:id/consumers/:consumer_id', consumerController.remove));

console.log("Server started.");
app.listen(3000);