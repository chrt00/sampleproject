﻿'use strict';

module.exports = Job
function Job(url, message) {
    this.attempts = 0;
    this.url = url;
    this.message = message;
    this.toString = function () {
        return JSON.stringify(this);
    }
}