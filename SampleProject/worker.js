/**
 * worker.js
 * Background queue for simple queueing system.
 * Does not guarantee in-order delivery. 
 * For example: [1, 2, 3, 4, 5] can be [3, 4, 5, 1, 2] due to simplistic retry policy.
 */

'use strict'
const request = require('request');
const Job = require('./job.js');
const JOB_THROTTLE = 1000;
var queue = [];

var doRequest = function () {
    var item = queue.pop();
    item.attempts++;
    // Give up after 10 attempts
    if (item.attempts > 10) {
        console.log('Failed ' + (item.attempts - 1) + ' attempts, removing from queue: ' + item.url + " " + item.message);
        return;
    }
    var url = item.url;
    var body = { message: item.message };
    var options = {
        uri: url,
        json: true,
        body: body
    }
    request.post(options, function optionalCallback(err, httpResponse, body) {
        if (err) {
            console.log(err);
            return;
        }
        console.log("" + httpResponse.statusCode + " " + item.url + " attempt #" + item.attempts + " " + item.message);
        if (httpResponse.statusCode != 200) {
            queue.push(item);
        }
    });
};

var doWork = function () {
    if (queue.length > 0) {
        doRequest();
    }
};

process.on('message', function (data) {
    console.log("job received: " + data.url + " " + data.message);
    queue.push(new Job(data.url, data.message));
});
console.log("Worker started.");
setInterval(doWork, JOB_THROTTLE);