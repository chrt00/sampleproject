﻿'use strict';

module.exports = Error
function Error(description) {
    this.status = 'error';
    this.error = description;
    this.toString = function () {
        return JSON.stringify(this);
    }
}